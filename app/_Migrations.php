<?php
namespace App;
use Schema;
use DB;
use Hash;

class _Migrations {

	function __construct() {	
		$this->_check_db_exists();
		// Create default tables
		$this->_create_table_admin();
		$this->_create_table_admin_group();
		$this->_create_table_adminlog();
		$this->_create_table_setting();
		$this->_create_table_throtte();
		$this->_create_table_channels();
		$this->_create_table_tags();
		$this->_create_table_users();
		$this->_create_table_posts();
		$this->_create_table_post_tags();
		$this->_create_table_media();
		$this->_create_table_slideshow();
	}

	/* Check DB */
	function _check_db_exists()
	{
		try{
			DB::connection()->getPdo();
		} catch(\Exception $e){
			die("Could not connect to the database : '".env('DB_DATABASE')."'. Please check your configuration.");
		}
	}
	
	/* Tbl Admin */
	function _create_table_admin()
	{
		$r = "
		CREATE TABLE `admin` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `admin_group_id` int(10) unsigned DEFAULT NULL,
		  `username` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
		  `password` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
		  `avatar` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
		  `name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
		  `email` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
		  `is_superadmin` tinyint(1) DEFAULT '0',
		  `remember_token` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
		  `recovery_token` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
		  `login_session` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
		  `created_at` datetime DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL,
		  `status` tinyint(1) unsigned DEFAULT '1',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if(!Schema::hasTable('admin')){
			DB::statement($r);
		}
	}
	
	function _create_table_admin_group(){
		$r = "CREATE TABLE IF NOT EXISTS `admin_group` (
			`id` int(21) unsigned NOT NULL AUTO_INCREMENT,
			`name` varchar(100) NULL DEFAULT '',
			`parent` int(11) unsigned NULL DEFAULT '0',
			`restrical_access` text NULL,
			`params` text NULL,
			`date` int(14) unsigned NULL DEFAULT '0',
			`created_at` datetime DEFAULT NULL,
			`updated_at` datetime DEFAULT NULL,
			`status` tinyint(1) unsigned NOT NULL DEFAULT '1',
			PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if(!Schema::hasTable('admin_group')){
			DB::statement($r);
		}
	}
	
	function _create_table_adminlog($execute = 1, $date = ''){
		$prefix = '';
		// $table = $prefix . ($date ? $date : date("Ymd"));
		$table = 'admin_logs_' . ($date ? $date : date("Ymd"));
		$r = "CREATE TABLE IF NOT EXISTS `$table` (
			  `".$prefix."id` int(21) unsigned NOT NULL AUTO_INCREMENT,
			  `".$prefix."admin_id` int(21) unsigned NULL DEFAULT '0',
			  `".$prefix."method` varchar(15) NULL,
			  `".$prefix."name` varchar(255) NULL DEFAULT '',
			  `".$prefix."description` mediumtext NULL,
			  `".$prefix."ip` varchar(255) NULL DEFAULT '',
			  `".$prefix."param` mediumtext NULL,
			  `".$prefix."created_at` datetime DEFAULT NULL,
			  `".$prefix."updated_at` datetime DEFAULT NULL,
			  PRIMARY KEY (`".$prefix."id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if (!$execute) { return $r; }
		if(!Schema::connection('logs')->hasTable($table)){
			DB::connection('logs')->statement($r);
		}
	}
	
	/* Setting */
	function _create_table_setting(){
		$r = "CREATE TABLE IF NOT EXISTS `setting` (
			`id` int(21) unsigned NOT NULL AUTO_INCREMENT,
			`setting_name` varchar(255) NULL DEFAULT '',
			`setting_value` text NULL,
			`created_at` datetime DEFAULT NULL,
			`updated_at` datetime DEFAULT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if(!Schema::hasTable('setting')){
			DB::statement($r);
		}
	}
	
	function _create_table_throtte(){
		$r = "CREATE TABLE `throttle` (
		  `session` varchar(255) CHARACTER SET latin1 NOT NULL,
		  `ip` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
		  `try` int(10) unsigned DEFAULT '0',
		  `attempt_lists` text CHARACTER SET latin1,
		  `start_time` int(10) unsigned DEFAULT NULL,
		  `end_time` int(10) unsigned DEFAULT NULL,
		  `status` tinyint(1) DEFAULT '1',
		  PRIMARY KEY (`session`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if(!Schema::hasTable('throttle')){
			DB::statement($r);
		}
	}
		
	function _create_table_channels(){
		$r = "
		CREATE TABLE `channels` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `parent_id` int(10) unsigned DEFAULT '0',
		  `name` varchar(100) DEFAULT NULL,
		  `description` mediumtext,
		  `is_top` tinyint(1) DEFAULT '0',
		  `params` mediumtext,
		  `order_num` int(10) unsigned DEFAULT '9999',
		  `created_at` datetime DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL,
		  `deleted_at` datetime DEFAULT NULL,
		  `status` tinyint(1) DEFAULT '1',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if(!Schema::hasTable('channels')){
			DB::statement($r);
		}
	}
	function _create_table_tags(){
		$r = "
		CREATE TABLE `tags` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(100) DEFAULT NULL,
		  `is_trending` tinyint(1) unsigned DEFAULT '0',
		  `view_count` int(10) unsigned DEFAULT '1',
		  `created_at` datetime DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL,
		  `status` tinyint(1) unsigned DEFAULT '1',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if(!Schema::hasTable('tags')){
			DB::statement($r);
		}
	}	
	
	function _create_table_users(){
		$r = "
		CREATE TABLE `users` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `email` varchar(100) DEFAULT NULL,
		  `username` varchar(100) DEFAULT NULL,
		  `password` varchar(200) DEFAULT NULL,
		  `name` varchar(50) DEFAULT NULL,
		  `gender` varchar(10) DEFAULT NULL,
		  `remember_token` varchar(100) DEFAULT NULL,
		  `reg_via` varchar(100) DEFAULT 'WEB',
		  `created_at` datetime DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL,
		  `deleted_at` datetime DEFAULT NULL,
		  `status` tinyint(1) unsigned DEFAULT '1',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if(!Schema::hasTable('users')){
			DB::statement($r);
		}
	}
	
	function _create_table_posts(){
		$r = "
		CREATE TABLE `posts` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `channel_id` int(10) unsigned DEFAULT NULL,
		  `title` varchar(255) DEFAULT NULL,
		  `synopsis` varchar(255) DEFAULT NULL,
		  `content` text,
		  `cover` varchar(255) DEFAULT NULL,
		  `keywords` varchar(255) DEFAULT NULL,
		  `admin_id` int(10) unsigned DEFAULT NULL,
		  `view_count` int(10) unsigned DEFAULT '1',
		  `params` mediumtext,
		  `created_at` datetime DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL,
		  `status` tinyint(1) DEFAULT '1',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if(!Schema::hasTable('posts')){
			DB::statement($r);
		}
	}
	
	function _create_table_post_tags(){
		$r = "
		CREATE TABLE `post_tags` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `post_id` int(10) unsigned DEFAULT NULL,
		  `tag_id` int(10) unsigned DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if(!Schema::hasTable('post_tags')){
			DB::statement($r);
		}
	}
	
	function _create_table_media(){
		$r = "
		CREATE TABLE `media` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `type` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
		  `title` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
		  `thumb` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
		  `bigimg` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
		  `path` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
		  `size` int(11) DEFAULT NULL,
		  `ext` char(4) CHARACTER SET latin1 DEFAULT NULL,
		  `created_at` datetime DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL,
		  `deleted_at` datetime DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

		";
		if(!Schema::hasTable('media')){
			DB::statement($r);
		}
	}
		
	function _create_table_slideshow(){
		$r = "
		CREATE TABLE `slideshow` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `type` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
		  `title` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
		  `description` mediumtext,
		  `thumb` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
		  `bigimg` varchar(255) DEFAULT NULL,
		  `path` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
		  `url` varchar(255) DEFAULT NULL,
		  `target` char(10) DEFAULT NULL,
		  `ordered_num` int(10) unsigned DEFAULT '0',
		  `created_at` datetime DEFAULT NULL,
		  `updated_at` datetime DEFAULT NULL,
		  `deleted_at` datetime DEFAULT NULL,
		   `status` tinyint(1) unsigned DEFAULT '1',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
		";
		if(!Schema::hasTable('slideshow')){
			DB::statement($r);
		}
	}
		
		
		
		
}