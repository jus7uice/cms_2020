<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTag extends Model
{
    protected $guarded = [];
	
	public function tag()
    {
        return $this->belongsTo('\App\Tag', 'tag_id');
    }
	 
}
