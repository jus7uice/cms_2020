<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Setting;

class ThemeCtr extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$theme = Setting::where('setting_name','THEME')->pluck('setting_value')->first();
		$skin = Setting::where('setting_name','SKIN')->pluck('setting_value')->first();
		$skinLists = [
			'red'=>'Red','red-light'=>'Red Light',
			'blue'=>'Blue','blue-light'=>'Blue Light',
			'green'=>'Green','green-light'=>'Green Light',
			'purple'=>'Purple','purple-light'=>'Purple Light',
			'yellow'=>'Yellow','yellow-light'=>'Yellow Light',
			'black'=>'Black','black-light'=>'Black Light',
		];
		return view('backend.theme',compact('theme','skin','skinLists'));
    }
	
	function postData(Request $request)
	{
		// dd($request->all());
		
		/* Save to DB */
		DB::table('setting')->where('setting_name','THEME')->update([
			'setting_value' => $request->theme,
		]);
		DB::table('setting')->where('setting_name','SKIN')->update([
			'setting_value' => $request->skin,
		]);
			
		$request->session()->put('theme',$request->theme);
		$request->session()->put('skin',$request->skin);
		
		/* Redirc */
		return redirect()->back()->with('msg',trans('message.save.success'));
	}
}
