<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;
use Image;

use App\Slideshow;

class SlideshowCtr extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		return view('backend.slideshow');
    }
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		return view('backend.slideshow_create');
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'file' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		// files storage folder
		$dir = UPLOAD_PATH.'slideshow';	
		
		if ($request->hasFile('file'))
		{						
			/* MKDIR */
			if (!file_exists($dir)) {mkdir($dir, 0777, true);}
			/* SET DIR */
			$dir = $dir.'/';
			
			/* START UPLOAD */
			$path = $request->file->path();
			$extension = strtolower($request->file->getClientOriginalExtension());
			$filename = strtolower(create_slug(substr($request->file->getClientOriginalName(),0,-4)).'_'.time().'.'.$extension);
			$filename_thumb = strtolower(create_slug(substr($request->file->getClientOriginalName(),0,-4)).'_'.time().'_thumb.'.$extension);
			$fileupload = $dir.$filename;
			if ($extension == 'png'|| $extension == 'jpg' || $extension == 'bmp' || $extension == 'gif' || $extension == 'jpeg'|| $extension == 'pjpeg')
			{
					// debug('1. intervention');
					$big_img = Image::make($path);
					$big_img->widen(1200, function ($constraint) {$constraint->upsize();});				
					$big_img->save($dir.$filename, 90);	// save to local is success
								
					// image upload			
					$thumb_img = Image::make($path);
					$thumb_img->fit(400);
					$thumb_img->save($dir.$filename_thumb, 70);// save file
					
					DB::table('slideshow')
					->insert([
						'type' => 'SLIDE',
						'title' => $request->title,
						'description' => $request->description,
						'url' => $request->url,
						'target' => $request->target,						
						'thumb' => $filename_thumb,
						'bigimg' => $filename,
						'path' => $dir,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
					]);
				
			}
		}
		
		// /* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = Slideshow::where('type','SLIDE')->where('status','<',2);
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('thumbnail',function($row){
			$thumb = $row->path.$row->thumb;
			$big = $row->path.$row->bigimg;
			$img = "";
			if(file_exists($thumb))
			{
				$img = '<a href="'.url($big).'" class="fancybox"><img src="'.url($thumb).'" height="100" /></a>';
			}			
			return $img;
		})		
		->addColumn('lbl_detail',function($row){
			$lbl = ''.$row->title;
			$lbl .= '<br />'.$row->description;
			return $lbl;
		})
		->addColumn('status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'slideshow.edit?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.edit').'</a>
			
			';
			return $action;
		})
		->rawColumns(['chkbox','thumbnail','lbl_detail','status','action'])
		->make();
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit(Request $request)
    {
		/* Load by ajax only */
		// if(!$request->ajax()){return redirect('err/restrical.access');}
		$item = Slideshow::find($request->id);
		return view('backend.slideshow_edit',compact('item'));
    }
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'ordered_num' => 'numeric',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* Save to DB */
		$row = Slideshow::find($request->id);
		$row->fill($request->all());
		$row->save();
				
		// /* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
		
	}
	 /**
     * Delete resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDelete(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* If group exist */
		DB::table('slideshow')->whereIn('id',$request->deleteItems)->update(['status'=>3]);
		
		/* Response */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.delete.success'));	
    }	
}
