<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Validator;
use DB;
use Datatables;
use Illuminate\Database\Eloquent\Collection;
use App\Setting;

class IpWhitelistCtr extends Controller
{
	/**
	* Setting . IP Block Data
	* @return Response
	*/
	function getData(Request $request)
	{
		/* Load by ajax only */
		if(!$request->ajax()){return die('restrical.access');}
		
		$fetch = DB::table('setting')->where('setting_name','IP_WHITELIST')->first();
		$data = json_decode($fetch->setting_value);		
		$col = new Collection;

		foreach ($data as $key => $value) {
	      $col->push([
	        'ip'=>'<div>'.$value.'</div>',
			'chkbox'=>'<input type="checkbox" name="deleteItems[]" value="'.$value.'" />'
	      ]);
	    }

	    return Datatables::of($col)
	    ->rawColumns(['ip','chkbox'])
	    ->make(true);
	}
	
	/**
	* Setting . IP Block
	* @return Response
	*/
	function index(Request $request)
	{
		if(!Setting::where('setting_name','IP_WHITELIST')->first()){
			Setting::create(['setting_name'=>'IP_WHITELIST','setting_value'=>'[]']);
		}
		return view('backend.ip_whitelist');
	}
	
	/**
	* Setting . Create IP Block
	* @return Response
	*/
	function getCreate(Request $request)
	{		
		return view('backend.ip_whitelist_create');
	}
	
	/**
	* Setting . IP Block . Post
	* @return Response
	*/
	function postCreate(Request $request)
	{
		/* Validate */
		$validator = Validator::make($request->all(), [
			'ip_address' => 'required|ip',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* Normal POST */
		$fetch = DB::table('setting')->where('setting_name','IP_WHITELIST')->first();
		$data = json_decode($fetch->setting_value);
		if(!in_array($request->ip_address, $data)){
			$data = json_encode(array_prepend($data, $request->ip_address));
			DB::table('setting')->where('setting_name','IP_WHITELIST')->update(['setting_value'=>$data]);
		}
		$setting = DB::table('setting')->where('setting_name','IP_WHITELIST')->first();
		
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
		
	}
	
	/**
	* Setting . IP Block . Delete
	* @return Response
	*/
	function postDelete(Request $request)
	{
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if (!$validator->passes()) {
			if($request->ajax()){
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* Normal POST */
		$fetch = DB::table('setting')->where('setting_name','IP_WHITELIST')->first();
		$data = json_decode($fetch->setting_value,true);
		
		/* Find & unset array value */
		$datax = array_diff($data, $request->deleteItems);		
		$data = array_flatten($datax);
		$data = json_encode($data);
		//dd($data);		//	["127.0.0.12","127.0.0.11","127.0.0.9","127.0.0.8","127.0.0.7","127.0.0.6","127.0.0.5","127.0.0.4","127.0.0.3","127.0.0.2"]
		DB::table('setting')->where('setting_name','IP_WHITELIST')->update(['setting_value'=>$data]);
	
		
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.update.success'));	
	}
	
	
	
}
