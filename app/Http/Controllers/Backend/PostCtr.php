<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;

use App\Post;
use App\Channel;
use App\PostTag;

class PostCtr extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('backend.post');
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = Post::with('channel')->where('posts.status','<',2)->select('posts.*');
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('lbl_post_tag',function($row){
			$fetch = DB::table('post_tags')->leftJoin('tags','tags.id','=','post_tags.tag_id')->where('post_tags.post_id',$row->id)->pluck('tags.name')->toArray();
			return implode(', ',$fetch);
		})
		->addColumn('status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'post.edit?id='.$row->id).'">'.trans('general.button.edit').'</a>
			
			';
			return $action;
		})
		->rawColumns(['chkbox','lbl_post_tag','status','action'])
		->make();
    }
	
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		$channel = Channel::where('status','<',2)->pluck('name','id')->toArray();
		$channelList = [' '=>'Select Channel :'] + $channel;
		return view('backend.post_create',compact('channelList'));
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {		
		/* Validate */
		$request->validate([
			'title' => 'required|unique:posts,title',
			'channel_id' => 'required',
			'content' => 'required',
		]);		
		
		/* Save to DB */
		$row = new Post;
		$row->fill($request->all());
		$row->content = str_encode($request->content);
		$row->save();
		
		if(count($request->tag) > 0){
			$data = [];
			foreach($request->tag as $tag){
				$data[] = ['post_id'=>$row->id,'tag_id'=>$tag];
			}
			PostTag::insert($data);
		}
				
		/* Redirc */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit(Request $request)
    {
		$item = Post::find($request->id);
		$channel = Channel::where('status','<',2)->pluck('name','id')->toArray();
		$channelList = [' '=>'Select Channel :'] + $channel;
		
		$postTag = PostTag::with('tag')->where('post_tags.post_id',$request->id)->get();
		$PostTagList = [];
		foreach($postTag as $row){
			$PostTagList = $PostTagList + [$row->tag_id => $row->tag->name];
		}
		
		// die(debug($PostTagList));
		return view('backend.post_edit',compact('item','channelList','PostTagList'));
    }
	
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		/* Validate */
		// $validator = Validator::make($request->all(), [
		$request->validate([
			'title' => [
				'required',
				Rule::unique('posts')->ignore($request->id),
			],
			'channel_id' => 'required',
			'content' => 'required',
		]);
				
		// dd($request->all());
		/* Save to DB */
		$row = Post::find($request->id);
		$row->fill($request->all());
		$row->content = str_encode($request->content);
		$row->save();
				
		if(count($request->tag) > 0){
			DB::table('post_tags')->where('post_id',$request->id)->delete();
			$data = [];
			foreach($request->tag as $tag){
				$data[] = ['post_id'=>$row->id,'tag_id'=>$tag];
			}
			PostTag::insert($data);
		}
		
		// /* Redirc */
		/* Redirc */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
}
