<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Auth;
use DB;
use Validator;
use Datatables;
use Route;

use App\Admin;
use App\AdminGroup;

class AdminGroupCtr extends Controller
{
		
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		return view('backend.admin_group');
    }

  
	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getGroupsData(Request $request)
    {		
		$rows = AdminGroup::where('status',1);
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('restrical_access',function($row){
			if(strlen($row->restrical_access)>2) return implode(", ",json_decode($row->restrical_access)); else return "";
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'admin.group.edit?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.edit').'</a>
			
			';
			return $action;
		})
		->rawColumns(['chkbox','restrical_access','action'])
		->make();
    }

	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createGroup(Request $request)
    {
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}

		/* Collect routes */
		$routes = Route::getRoutes();
		// $routeLists = [''=>'[None]'];
		$routeLists = [];
		foreach ($routes as $route)
		{
			if($route->methods[0] == "GET"){
				$uri = str_replace(ADMIN_PATH,'',$route->uri);
				$pos = strpos($uri, '/');
				if ($pos === false) {
					$pos2 = strpos($uri, '.data');
					if ($pos2 === false) {
						$routeLists = $routeLists + [$uri => $uri];
					}
				}
			}
		}
		
		/* Exclude from Array Route */
		$exclude_admin_path_root = substr(ADMIN_PATH,0,strlen(ADMIN_PATH)-1); //debug($exclude_admin_path_root);
		if(isset($routeLists[$exclude_admin_path_root])) unset($routeLists[$exclude_admin_path_root]);
		if(isset($routeLists['logout'])) unset($routeLists['logout']);
		
		return view('backend.admin_group_create',compact('routeLists'));
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreateGroup(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'name' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		if(empty($request->route)) $routes = "[]"; else $routes = json_encode($request->route);
		
		// /* Save to DB */
		$row = new AdminGroup;
		$row->name = $request->name;
		$row->restrical_access = $routes;
		$row->save();
		
		/* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editGroup(Request $request)
    {	
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		/* Collect routes */
		$routes = Route::getRoutes();
		// $routeLists = [''=>'[None]'];
		$routeLists = [];
		foreach ($routes as $route)
		{
			if($route->methods[0] == "GET"){
				$uri = str_replace(ADMIN_PATH,'',$route->uri);
				$pos = strpos($uri, '/');
				if ($pos === false) {
					$pos2 = strpos($uri, '.data');
					if ($pos2 === false) {
						$routeLists = $routeLists + [$uri => $uri];
					}
				}				
			}
			
		}
		/* Exclude from Array Route */
		$exclude_admin_path_root = substr(ADMIN_PATH,0,strlen(ADMIN_PATH)-1); //debug($exclude_admin_path_root);
		if(isset($routeLists[$exclude_admin_path_root])) unset($routeLists[$exclude_admin_path_root]);
		if(isset($routeLists['logout'])) unset($routeLists['logout']);
		// debug($routeLists);
		
		/* get resource */
		$item = AdminGroup::find($request->id);
		
		return view('backend.admin_group_edit',compact('item','routeLists'));
    }
	
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEditGroup(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'name' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		if(empty($request->route)) $routes = "[]"; else $routes = json_encode($request->route);
		
		/* Update Resource */
		$row = AdminGroup::find($request->id);
		$row->name = $request->name;
		$row->restrical_access = $routes;
		$row->save();
		
		/* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	
	 /**
     * Delete resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDeleteGroups(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* If group exist */
		DB::table('admin_group')->whereIn('id',$request->deleteItems)->update(['status'=>0]);
		
		/* Response */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.delete.success'));	
    }
	
	
}
