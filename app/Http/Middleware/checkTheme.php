<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class checkTheme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->session()->has('theme'))
		{
			$theme = DB::table('setting')->where('setting_name','THEME')->pluck('setting_value')->first();
			/* Set global Session */
			$request->session()->put('theme',$theme);
		}		
		
		if(!$request->session()->has('skin'))
		{
			$skin = DB::table('setting')->where('setting_name','SKIN')->pluck('setting_value')->first();
			/* Set global Session */
			$request->session()->put('skin',$skin);
		}		

		return $next($request);
    }
}
