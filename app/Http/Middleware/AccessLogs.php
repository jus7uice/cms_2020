<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Auth;
use Schema;

class AccessLogs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* Get Current Route */
		$user = Auth::guard('admin')->user();
		
		if (Auth::guard('admin')->check())
		{
			/* Save to Logs */
			$table = 'admin_logs_' . date("Ymd");
			
			if(!strstr($request->route()->uri(),'.data') && !strstr($request->route()->uri(),'.log')){
				if(
					strstr($request->route()->uri(),'.create') || 
					strstr($request->route()->uri(),'.edit') || 
					strstr($request->route()->uri(),'.delete')
				){
				
					$values = [
						'admin_id' => $user->id,
						'method' => $request->route()->methods[0],
						'name' => $user->username,
						'description' => $request->fullUrl(),
						'ip' => $request->ip(),
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
					];
			
					// debug($values);
					if(Schema::connection('logs')->hasTable($table)){
						DB::connection('logs')->table($table)->insert($values);
					}
				}
			
			}
		}
		
		return $next($request);
    }
}
