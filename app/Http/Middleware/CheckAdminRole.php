<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use View;
use App\Admin;

class CheckAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$path = str_replace(ADMIN_PATH,'',$request->path());
		
		if(!$request->session()->has('checkRole'))
		{
			$user = Auth::guard('admin')->user();
			
			/* Load Admin Restrical Access */
			$result = Admin::with('group')->where('admin.id',$user->id)->first();
			$request->session()->put('checkRole',json_decode($result->group->restrical_access));
		}
		// $checkAccess = json_decode($result->group->restrical_access);
		$checkAccess = session('checkRole');
		if(in_array($path,$checkAccess))
		{
			return redirect(ADMIN_PATH.'err/restrical.access');
		}	
		
		/* set token wallet in attribut */
		$request->attributes->add(['admin_role_array' => $checkAccess]);
		
		View::share('path', $path);
        return $next($request);
    }
}
