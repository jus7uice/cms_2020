<?php

namespace App\Http\Middleware;

use Closure;
use DB;
// use App\Setting;

class CheckIpWhitelists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$IpWhitelist = DB::table('setting')->where('setting_name','IP_WHITELIST')->pluck('setting_value')->first();
		if($IpWhitelist && in_array($request->ip(),json_decode($IpWhitelist)))
		{
			return $next($request);
		}	
		
		return response()->json([
			'error'=>"1",
			'message'=>"Kamu tidak memiliki otoritas untuk mengakses sumber ini",
			'ip'=>$request->ip(),
		]);
		
		// return redirect('err/ip.blocked');
    }
}
