@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- Restrical Username
@endsection

{{-- Page title --}}
@section('pagetitle')
	Restrical Username</small>
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-6">		
			<p><a class="btn btn-primary" href="{{url(ADMIN_PATH.'restrical.username.create')}}" data-toggle="ajaxModal" >{{trans('general.label.add_new')}}</a></p>
        </div>
		
		<div class="col-md-12">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'restrical.username.delete')}}" method="post" id="ajxFormDelete">
				<div class="box-header with-border">
				  <h3 class="box-title">Restrical Username</h3>
				</div>
				<div class="box-body">
				
					<table id="dataTbl" class="table table-bordered table-hover datatable" data-ajax="{{url(ADMIN_PATH.'restrical.username.data')}}" data-processing="true" data-server-side="true" data-orderingxxx="false" data-length-menu="[50,100,500]">
						<thead>
						<tr>
						  <th data-data="chkbox" data-orderable="false"><input type="checkbox" id="select-all" width="20" /></th>
						  <th data-data="ip">Restrical Username Word</th>
						</tr>
						</thead>
						
					</table>
				
				</div>
				 <div class="box-footer">
					<button type="submit" class="btn btn-danger">{{trans('general.button.delete')}}</button>
					 {{csrf_field()}}
				</div>
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection