@extends('layouts.backend')
@section('body-class')
hold-transition login-page
@endsection

@section('body')

@php
// echo time();
// if(request()->session()->has('throttle'))
// {
	// echo session('throttle');
// }	
// echo session('attempt_lists');
// print_r(session()->all());
@endphp

<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>{{config('app.name')}}</b> Panel</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Your IP : {{Request()->ip()}}</p>
	
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif
	
	@if (session('msg'))
    <div class="alert alert-success">
        {{ session('msg') }}
    </div>
	@endif
	
	
	@if($isThottle > 0)
		<div class="alert alert-danger">{{trans('auth.throttle',["seconds"=>60])}}</div>
	@else
    <form action="{{url(ADMIN_PATH.'authadmin')}}" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" value="{{Request()->old('username')}}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
         
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">{{trans('general.button.submit')}}</button>
        </div>
        <!-- /.col -->
      </div>
	  {{csrf_field()}}
    </form>
	@endif

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@endsection