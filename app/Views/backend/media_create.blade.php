<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'media.create').'" method="post" id="ajxFormXXX" enctype="multipart/form-data">
  <div class="box-body">
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.title').' *</span>
	  <input type="text" class="form-control" name="title">
	</div>
	
	<div class="fileinput fileinput-new" data-provides="fileinput">
	  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
	  <div>
		<span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="file"></span>
		<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
	  </div>
	</div>
		
				
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.add_new').' | '. trans('general.media.media'), 'body'=>$body])