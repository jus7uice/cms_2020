<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'channel.edit').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.name').' *</span>
	  <input type="text" class="form-control" name="name" value="'.$item->name.'">
	</div>
	
	<div class="input-group">
		 <span class="input-group-addon">'.trans('general.label.parent').'</span>
	   '.Form::select('parent_id', $channelList, $item->parent_id,['class'=>'form-control']).'
	</div>
	
	<div class="form-group">
	   '.Form::hidden('is_top',0).'
	   '.Form::checkbox('is_top',1,($item->is_top==1)?true:false).'
		 <label>'.trans('general.label.is_top').' *</label>
	</div>
			
	<div class="form-group">
	   '.Form::hidden('status',0).'
		'.Form::checkbox('status',1,($item->status==1)?true:false).'
		 <label>'.trans('general.label.is_active').' *</label>
	</div>
			
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
	  '.Form::hidden('id',request()->id).'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.edit').' | '. trans('general.channel.channel'), 'body'=>$body])