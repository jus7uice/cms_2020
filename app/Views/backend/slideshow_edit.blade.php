<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'slideshow.edit').'" method="post" id="ajxForm" enctype="multipart/form-data">
  <div class="box-body">
	
	<div class="form-group">
		<img src="'.url($item->path.$item->thumb).'" height="100" />
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.title').' *</span>
	  <input type="text" class="form-control" name="title" value="'.$item->title.'" />
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.description').'</span>
	  <textarea class="form-control" name="description">'.$item->description.'</textarea>
	</div>
	
	<hr />
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.url').' (Optional)</span>
	  <input type="text" class="form-control" name="url" value="'.$item->url.'" />
	</div>
	
	<div class="input-group">
		 <span class="input-group-addon">'.trans('general.label.target').'</span>
	   '.Form::select('target',[''=>'Open in same page','_blank'=>'Open in new page'], $item->target,['class'=>'form-control']).'
	</div>
		
	<hr />
	
	<div class="input-group">
		<span class="input-group-addon">'.trans('general.label.ordered_num').'</span>
		<input type="text" class="form-control" name="ordered_num" value="'.$item->ordered_num.'" />
	</div>
	
		
	<div class="form-group">
	   '.Form::hidden('status',0).'
		'.Form::checkbox('status',1,($item->status==1)?true:false).'
		 <label>'.trans('general.label.is_active').' *</label>
	</div>
				
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
	  '.Form::hidden('id',request()->id).'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.edit').' | '. trans('general.label.slideshow'), 'body'=>$body])