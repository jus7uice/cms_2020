<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'admin.user.edit').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
		 <span class="input-group-addon">'.trans('general.people.group').' *</span>
	   '.Form::select('group', $groupList, $item->admin_group_id,['class'=>'form-control']).'
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.username').' *</span>
	  <input type="text" class="form-control" name="username" value="'.$item->username.'" />
	</div>
	
	<hr />
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.name').' *</span>
	  <input type="text" class="form-control" name="name" value="'.$item->name.'" />
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.email').' *</span>
	  <input type="email" class="form-control" name="email" value="'.$item->email.'" />
	</div>
	
	<hr />
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.change_password').'</span>
	  <input type="password" class="form-control" name="password" value="" />
	</div>
	<span class="help-block text-orange">'.trans('message.comment_change_password').' *</span>
	
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
	    '.Form::hidden('id',Request()->id).'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.edit').' | '.trans('general.label.user'), 'body'=>$body])