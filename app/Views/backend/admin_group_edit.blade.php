<?php 
/*  Form::select('route[]', $routeLists, json_decode($item->restrical_access),['class'=>'form-control multisel','multiple']) */
$chkBox="";
foreach($routeLists as $key=>$val){	
	if(isset($item->restrical_access) && in_array($val,json_decode($item->restrical_access))){
		$chkBox .= '<label>'.Form::checkbox('route[]',$key,true).' '.$val.'</label>';
	} else {
		$chkBox .= '<label>'.Form::checkbox('route[]',$key).' '.$val.'</label>';		
	}
	$chkBox .= ' <br />';
}


$param = json_decode($item->params);

$body = '

		<form role="form" action="'.url(ADMIN_PATH.'admin.group.edit').'" method="post" id="ajxForm">
		  <div class="box-body">
		  
			<div class="input-group">
			  <span class="input-group-addon">'.trans('general.people.name').' *</span>
			  <input type="text" class="form-control" name="name" value="'.$item->name.'">
			</div>

			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab-general">Restrical Access</a></li>
				<li><a data-toggle="tab" href="#tab-bank">Bank</a></li>
			</ul>
			<p>&nbsp;</p>
			
			<div class="tab-content">
				<div id="tab-general" class="tab-pane fade in active">
					
					<div class="callout callout-info">
					'.trans('general.about.restrical_access').'
					</div>  
					<div class="input-group">
					  <span class="input-group-addon">Restrical Access</span>
						<div class="scroll-box">
							<div class="checkbox">
							'.$chkBox.'
							</div>
						</div>
					</div>
					
				</div>
				
				<div id="tab-bank" class="tab-pane fade in">
				
					BANK
					
				</div>
				
				
			</div><!-- /tab-content -->
			
			
			
			
			
			
			
			
			
			
			
			
			
					
		  </div>
		  <!-- /.box-body -->

		  <div class="box-footer">
			<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
			  '.csrf_field().'
			  '.Form::hidden('id',Request()->id).'
		  </div>
		  
		    
		</form>
';
?>

@include('modal.modal',['title'=>trans('general.label.edit').' | '.trans('general.people.group'), 'body'=>$body])