@php
if(!isset($path)) $path="/";
$admin_role_array = Request()->get('admin_role_array');
@endphp

<div class="navbar-header">
 <a href="{{url(ADMIN_PATH)}}" class="navbar-brand"><b>{{config('app.name')}}</b> Panel</a>
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
	<i class="fa fa-bars"></i>
  </button>
</div>


<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
  <ul class="nav navbar-nav">
	<li class="{{(strstr('dashboard',$path)?'active':'')}}"><a href="{{url(ADMIN_PATH.'dashboard')}}">{{trans('general.dashboard')}}</a></li>
	@if(role_menu($admin_role_array,'tag')) <li class="{{(strstr('tag',$path)?'active':'')}}"><a href="{{url(ADMIN_PATH.'tag')}}">{{trans('general.tag.tag')}}</a></li> @endif
	
	<!--
	<li><a href="#">Link</a></li>
	-->
	
  
	<li class="dropdown {{(in_array($path,['post','post.create','channel'])?'active':'')}}">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{trans('general.post.post')}} <span class="caret"></span></a>
	  <ul class="dropdown-menu" role="menu">
		@if(role_menu($admin_role_array,'post')) <li><a href="{{url(ADMIN_PATH.'post')}}">{{trans('general.post.post')}}</a></li> @endif
		@if(role_menu($admin_role_array,'channel')) <li><a href="{{url(ADMIN_PATH.'channel')}}">{{trans('general.channel.channel')}}</a></li> @endif
	  </ul>
	</li>	
  
	<li class="dropdown {{(in_array($path,['media'])?'active':'')}}">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{trans('general.media.media')}} <span class="caret"></span></a>
	  <ul class="dropdown-menu" role="menu">
		@if(role_menu($admin_role_array,'media')) <li><a href="{{url(ADMIN_PATH.'media')}}">{{trans('general.media.image_manager')}}</a></li> @endif
	  </ul>
	</li>	
	
	<li class="dropdown {{(in_array($path,['user','user.create'])?'active':'')}}">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{trans('general.user.user')}} <span class="caret"></span></a>
	  <ul class="dropdown-menu" role="menu">
		@if(role_menu($admin_role_array,'user')) <li><a href="{{url(ADMIN_PATH.'user')}}">{{trans('general.user.user')}}</a></li> @endif
		<li class="divider"></li> 
		@if(role_menu($admin_role_array,'restrical.username')) <li><a href="{{url(ADMIN_PATH.'restrical.username')}}">Restrical Username</a></li> @endif
	  </ul>
	</li>	
  
	<li class="dropdown {{(in_array($path,['setting','setting.ipblock','setting.theme','slideshow'])?'active':'')}}">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{trans('general.setting.setting')}} <span class="caret"></span></a>
	  <ul class="dropdown-menu" role="menu">
		<!--
		<li><a href="#">Action</a></li>
		<li><a href="#">Another action</a></li>
		<li><a href="#">Something else here</a></li>
		<li class="divider"></li>
		<li><a href="#">Separated link</a></li>
		<li class="divider"></li>
		-->
		@if(role_menu($admin_role_array,'setting.ipwhitelist'))<li><a href="{{url(ADMIN_PATH.'setting.ipwhitelist')}}">{{trans('general.setting.ipwhitelist')}}</a></li> @endif	
		<li class="divider"></li> 
		@if(role_menu($admin_role_array,'setting.theme')) <li><a href="{{url(ADMIN_PATH.'setting.theme')}}">{{trans('general.label.theme')}}</a></li> @endif
		@if(role_menu($admin_role_array,'slideshow')) <li><a href="{{url(ADMIN_PATH.'slideshow')}}">{{trans('general.label.slideshow')}}</a></li> @endif
	  </ul>
	</li>	  
	
	
	<li class="dropdown {{(in_array($path,['admin.user','admin.group'])?'active':'')}}">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{trans('general.admin.admin')}} <span class="caret"></span></a>
	  <ul class="dropdown-menu" role="menu">						
		@if(role_menu($admin_role_array,'admin.user')) <li><a href="{{url(ADMIN_PATH.'admin.user')}}">{{trans('general.admin.user_management')}}</a></li> @endif
		@if(role_menu($admin_role_array,'admin.group')) 
			<li><a href="{{url(ADMIN_PATH.'admin.group')}}">{{trans('general.admin.user_group')}}</a></li>
			<li class="divider"></li> 
		@endif
		@if(role_menu($admin_role_array,'admin.log')) <li><a href="{{url(ADMIN_PATH.'admin.log')}}">{{trans('general.admin.logs')}}</a></li> @endif

	  </ul>
	</li>
		
  </ul>
  <!--
  <form class="navbar-form navbar-left" role="search">
	<div class="input-group">
	  <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
	</div>
  </form>
  -->
</div>
<!-- /.navbar-collapse -->

		
<!-- Navbar Right Menu -->
<div class="navbar-custom-menu">
  <ul class="nav navbar-nav">
	<!-- Messages: style can be found in dropdown.less-->
	<li class="dropdown messages-menu">
	  <!-- Menu toggle button -->
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<i class="fa fa-envelope-o"></i>
		<span class="label label-success">4</span>
	  </a>
	  <ul class="dropdown-menu">
		<li class="header">You have 4 messages</li>
		<li>
		  <!-- inner menu: contains the messages -->
		  <ul class="menu">
			<li><!-- start message -->
			  <a href="#">
				<div class="pull-left">
				  <!-- User Image -->
				  <img src="" class="img-circle" alt="User Image">
				</div>
				<!-- Message title and timestamp -->
				<h4>
				  Support Team
				  <small><i class="fa fa-clock-o"></i> 5 mins</small>
				</h4>
				<!-- The message -->
				<p>Why not buy a new awesome theme?</p>
			  </a>
			</li>
			<!-- end message -->
		  </ul>
		  <!-- /.menu -->
		</li>
		<li class="footer"><a href="#">See All Messages</a></li>
	  </ul>
	</li>
	<!-- /.messages-menu -->

	<!-- Notifications Menu -->
	<li class="dropdown notifications-menu">
	  <!-- Menu toggle button -->
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<i class="fa fa-bell-o"></i>
		<span class="label label-warning">10</span>
	  </a>
	  <ul class="dropdown-menu">
		<li class="header">You have 10 notifications</li>
		<li>
		  <!-- Inner Menu: contains the notifications -->
		  <ul class="menu">
			<li><!-- start notification -->
			  <a href="#">
				<i class="fa fa-users text-aqua"></i> 5 new members joined today
			  </a>
			</li>
			<!-- end notification -->
		  </ul>
		</li>
		<li class="footer"><a href="#">View all</a></li>
	  </ul>
	</li>
	<!-- Tasks Menu -->
	<li class="dropdown tasks-menu">
	  <!-- Menu Toggle Button -->
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<i class="fa fa-flag-o"></i>
		<span class="label label-danger">9</span>
	  </a>
	  <ul class="dropdown-menu">
		<li class="header">You have 9 tasks</li>
		<li>
		  <!-- Inner menu: contains the tasks -->
		  <ul class="menu">

			<li><!-- Task item -->
			  <a href="#">
				<!-- Task title and progress text -->
				<h3>
				  Design some buttons
				  <small class="pull-right">20%</small>
				</h3>
				<!-- The progress bar -->
				<div class="progress xs">
				  <!-- Change the css width attribute to simulate progress -->
				  <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
					<span class="sr-only">20% Complete</span>
				  </div>
				</div>
			  </a>
			</li>
			<!-- end task item -->
			
		  </ul>
		</li>
		<li class="footer">
		  <a href="#">View all tasks</a>
		</li>
	  </ul>
	</li>
	<!-- User Account Menu -->
	<li class="dropdown user user-menu {{(strstr('admin.profile',$path)?'active':'')}}">
	  <!-- Menu Toggle Button -->
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<!-- The user image in the navbar-->
		<img src="{{asset('adminlte/avatar/_avatar.png')}}" class="user-image" alt="User Image">
		<!-- hidden-xs hides the username on small devices so only the image appears. -->
		<span class="hidden-xs">{{(isset($auth_admin)?$auth_admin->name:'')}}</span> <span class="caret"></span>
	  </a>
	  <ul class="dropdown-menu">
		<!--
		<li class="header"></li>
		-->
		<li><a href="{{url(ADMIN_PATH.'admin.profile')}}"><i class="fa fa-user"></i> {{trans('general.profile.title')}}</a></li>		
		<li class="divider"></li>
		<li><a href="{{url('/')}}" target="_blank"><i class="fa fa-plane"></i> {{trans('general.homepage')}}</a></li>
		<li class="divider"></li>
		<li><a href="{{url(ADMIN_PATH.'logout')}}"><i class="fa fa-sign-out"></i> {{trans('general.logout')}}</a></li>
			  
		<!-- The user image in the menu -->
		<?php
		/*
		<li class="user-header">
		  <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

		  <p>
			Alexander Pierce - Web Developer
			<small>Member since Nov. 2012</small>
		  </p>
		</li>
		<!-- Menu Body -->
		<li class="user-body">
		  <div class="row">
			<div class="col-xs-4 text-center">
			  <a href="#">Followers</a>
			</div>
			<div class="col-xs-4 text-center">
			  <a href="#">Sales</a>
			</div>
			<div class="col-xs-4 text-center">
			  <a href="#">Friends</a>
			</div>
		  </div>
		  <!-- /.row -->
		</li>
		*/ ?>
		<!-- Menu Footer-->
		<!--
		<li class="user-footer">
		  <div class="pull-left">							
		  </div>
		  <div class="pull-right">							
		  </div>
		</li>
		-->
	  </ul>
	</li>
  </ul>
</div>