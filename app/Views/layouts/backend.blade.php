<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{config('app.name')}} @yield('title')</title>
		<link rel="shortcut icon" href="{{asset('favicon.ico')}}">
        <!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.7 -->
		<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="{{asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
		<!-- Ionicons -->
		<link rel="stylesheet" href="{{asset('adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
		<!-- Datatables -->
		<link rel="stylesheet" href="{{asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
		<!-- Sans Pro Font -->
		<link rel="stylesheet" href="{{asset('fonts/Source_Sans_Pro_Light/font.css')}}">
		<!-- Theme style -->
		@yield('css')
		<link rel="stylesheet" href="{{asset('adminlte/dist/css/AdminLTE.min.css')}}">
		<!-- Jasny -->
		<link rel="stylesheet" href="{{asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{asset('plugins/bootstrap-multiselect/bootstrap-multiselect.css')}}">
		
		<!-- AdminLTE Skins. Choose a skin from the css/skins
		folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="{{asset('adminlte/dist/css/skins/_all-skins.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/app.css')}}">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	
	</head>
	
	<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
	<body class="@yield('body-class','hold-transition skin-'.session('skin').' layout-top-nav')">
	@section('body')
	
				
		<div class="wrapper">
		  <header class="main-header">
			<nav class="navbar navbar-static-top">
			  <div class="container-fluid">
					@include('layouts._top_nav')					
			  </div>
			</nav>
		  </header>
		  
				<div class="callout callout-danger clearfix no-margin">
				  <h4 class="inline">Perhatian!</h4>
				  <p>Ini hanyalah halaman untuk tujuan demo, pada penggunaan yang sebenarnya beberapa menu, konten, bahasa, warna, konfigurasi dan fitur lainnya bisa berbeda tergantung jenis website yang dibuat. :)</p>
				</div>
				
		  <!-- Full Width Column -->
		  <div class="content-wrapper">
			<div class="container-fluid">
				<section class="content-header">
				  <h1>
					@yield('pagetitle')
				  </h1>
				  <ol class="breadcrumb">
					@yield('breadcrumb')
				  <!--
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Forms</a></li>
					<li class="active">General Elements</li>
				  -->
				  </ol>
				</section>
				<section class="content">
					<!-- Show/Error message -->
					<div id="ajxForm_message"></div>
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif				
					
					@if (session()->has('msg'))	<div class="alert alert-success">{{ session()->get('msg') }}</div>@endif					
					@if (session()->has('msgError'))<div class="alert alert-danger">{{ session()->get('msgError') }}</div>@endif
					
					<!-- /Show/Error message -->
				@yield('content')
				</section>

			</div>
			<!-- /.container -->
		  </div>
		  <!-- /.content-wrapper -->
		  <footer class="main-footer">
			<div class="container-fluid">
			  <div class="pull-right hidden-xs">
				<b>Version</b> {{(config('app.app_version')?config('app.app_version'):'-') }} &nbsp;
				<b>Load Time</b> in {{round((microtime(true) - LARAVEL_START),3)}} seconds &nbsp;
				<b>IP</b> {{request()->ip()}}
			  </div>
			  <strong>Copyright &copy; 2017 <a href="#">{{config('app.copyright') }}</a>.</strong> All rights
			  reserved.
			</div>
			<!-- /.container -->
		  </footer>
		</div>
	@show	
		<!-- ./wrapper -->

		<!-- jQuery 3 -->
		<script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
		<!-- Bootstrap 3.3.7 -->
		<script src="{{asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
		<!-- SlimScroll -->
		<script src="{{asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
		<!-- FastClick -->
		<script src="{{asset('adminlte/bower_components/fastclick/lib/fastclick.js')}}"></script>
		<!-- Datatables -->
		<script src="{{asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
		<!-- Combodate -->
		<script src="{{asset('adminlte/bower_components/moment/min/moment.min.js')}}"></script>
		<script src="{{asset('plugins/combodate/combodate.js')}}"></script>
		<!-- Jasny -->
		<script src="{{asset('plugins/jasny-bootstrap/js/jasny-bootstrap.min.js')}}"></script>
		<!-- AdminLTE App -->
		<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
		<!-- Other -->
		<script src="{{asset('plugins/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
		<script src="{{asset('js/jquery.ajax.form.js')}}"></script>
		<script src="{{asset('js/jquery.nestable.js')}}"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="{{asset('adminlte/dist/js/demo.js')}}"></script>
		@yield('js')
		
		<script src="{{asset('js/app.js')}}"></script>
	</body>
	
</html>
