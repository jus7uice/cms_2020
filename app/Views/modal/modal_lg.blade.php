<!-- Modal -->
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">{{(isset($title)?$title:'')}}</h4>
  </div>
  <div class="modal-body">
	<div id="modal-message"></div>
	{!!(isset($body)?$body:'')!!}
  </div>
  @if(isset($footer))
  <div class="modal-footer">
	{{$footer}}
  </div>
  @endif
</div>
</div>
