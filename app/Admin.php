<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
	protected $table = 'admin';
	// protected $guarded = ['admin_id'];
	// protected $fillable = [
        // 'name', 'password', 'username', 'email', 'admin_group_id'
    // ];
	
	protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	
	public function group()
    {
        // return 'Admin Group';
		return $this->belongsTo('\App\AdminGroup', 'admin_group_id');
    }

}
