<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use DB;

class RecentAdminLogs extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
		'count'=>5,
	];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
		$user = Auth::guard('admin')->user();
        // Fetch DB
		$tabel = 'admin_logs_'.date("Ymd");
		$data = DB::connection('logs')->table($tabel)->where('admin_id',$user->id)->orderBy('id','DESC')->take($this->config['count'])->skip(1)->get();

        return view('widgets.recent_admin_logs', [
            'config' => $this->config,
            'data' => $data,
        ]);
    }
}
